.. calculs documentation master file, created by
   sphinx-quickstart on Tue Oct 17 14:35:49 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to calculs's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

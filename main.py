import unittest
# Importer le package de test


class MatriceDimensionError(Exception):
    """
    Creer une class qui contient la fonction qui vérifie les dimensions des matrices
    """
    def __init__(self, message="Les dimensions des matrices ne sont pas compatibles."):
        super().__init__(message)


def matrice(nb_lignes, nb_colonnes):
    """
    Créer une matrice de dimensions nb_lignes x nb_colonnes avec des zéros.
    """
    mat = []
    for i in range(nb_lignes):
        ligne = [0] * nb_colonnes
        mat.append(ligne)
    return mat


def change_valeur(matrice1, ligne, colonne, nouvelle_valeur):
    """
    Modifier une valeur dans de la matrice1 à la position (ligne, colonne).
    """
    if 0 <= ligne < len(matrice1) and 0 <= colonne < len(matrice1[0]):
        matrice1[ligne][colonne] = nouvelle_valeur
    else:
        print("Indices de ligne ou de colonne non valides.")
    return matrice1


def matrice_identite(taille):
    """
    Créer une matrice identité de taille donnée.
    """
    matrice2 = []
    for i in range(taille):
        ligne = [1 if i == j else 0 for j in range(taille)]
        matrice2.append(ligne)
    return matrice2


def somme_matrice(matrice1, matrice2):
    """
    Additionne deux matrices de même dimension.
    """
    if len(matrice1) != len(matrice2) or len(matrice1[0]) != len(matrice2[0]):
        raise MatriceDimensionError()
    somme = []
    for i in range(len(matrice1)):
        ligne = [matrice1[i][j] + matrice2[i][j] for j in range(len(matrice1[0]))]
        somme.append(ligne)
    return somme


def is_element(matrice12, nombre):
    """
    Trouve la position de nombre dans la matrice.
    """
    for x, ligne in enumerate(matrice12):
        for y, element in enumerate(ligne):
            if element == nombre:
                return x, y
    return None


def produit_scalaire(matrice23, nombre):
    """
    Multiplie chaque élément de la matrice par un nombre.
    """
    resultat = []
    for ligne in matrice23:
        nouvelle_ligne = [element * nombre for element in ligne]
        resultat.append(nouvelle_ligne)
    return resultat


def produit_matriciel(matrice1, matrice2):
    """
    Effectuer le produit matriciel de deux matrices
    en verifiant qu'elles soit de même dimension
    et mettre le resultat dans la liste "resultat"
    """
    lignes1, colonnes1 = len(matrice1), len(matrice1[0])
    lignes2, colonnes2 = len(matrice2), len(matrice2[0])

    if colonnes1 != lignes2:
        raise MatriceDimensionError()

    resultat = []
    for i in range(lignes1):
        ligne_resultat = []
        for j in range(colonnes2):
            produit = 0
            for k in range(colonnes1):
                produit += matrice1[i][k] * matrice2[k][j]
            ligne_resultat.append(produit)
        resultat.append(ligne_resultat)

    return resultat


class TestMatrice(unittest.TestCase):
    # class qui contient toute les fonctions de test unitaire

    def test_matrice_creation(self):
        """
        Tester la création de matrice tout à 0
        :return: list 3 lignes et 4 colonnes avec toutes les valeurs à 0
        """
        nb_lignes = 3
        nb_colonnes = 4
        ma_matrice = matrice(nb_lignes, nb_colonnes)
        self.assertEqual(ma_matrice, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])

    def test_change_valeur(self):
        """
        Tester le changment de valeur sur la ligne 1, colonne 1
        :return: valeur 10 sur la ligne 1, colonne 1
        """
        ma_matrice = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        nouvelle_matrice = change_valeur(ma_matrice, 1, 1, 10)
        self.assertEqual(nouvelle_matrice, [[1, 2, 3], [4, 10, 6], [7, 8, 9]])

    def test_matrice_identite(self):
        """
        Tester la création d'une matrice 3 lignes et 3 colonnes
        :return: Matrice 3 lignes et 3 colonnes
        """
        taille = 3
        matrice_id = matrice_identite(taille)
        self.assertEqual(matrice_id, [[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    def test_somme_matrice(self):
        """
        Tester la somme de 2 matrices
        :return: Résultat de la somme = 10 sur chaque valeur
        """
        matrice1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        matrice2 = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]
        resultat = somme_matrice(matrice1, matrice2)
        self.assertEqual(resultat, [[10, 10, 10], [10, 10, 10], [10, 10, 10]])

    def test_is_element(self):
        """
        Tester la recherche de la valeur 5
        :return: la position rechercher est ligne 1 et colonne 1
        """
        ma_matrice = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        nombre_recherche = 5
        position = is_element(ma_matrice, nombre_recherche)
        self.assertEqual(position, (1, 1))

    def test_produit_scalaire(self):
        """
        Tester la multiplication par 2 de chaque valeur
        :return: chaque valeur multiplié par 2
        """
        ma_matrice = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        facteur = 2
        resultat = produit_scalaire(ma_matrice, facteur)
        self.assertEqual(resultat, [[2, 4, 6], [8, 10, 12], [14, 16, 18]])

    def test_produit_matriciel(self):
        """
        Tester la multiplication des lignes avec les colonnes
        :return: (1x5)+(2x7), (1x6)+(2x8), (3x5)+(4x7), (3x6)+(4x8)
        """
        matrice1 = [[1, 2], [3, 4]]
        matrice2 = [[5, 6], [7, 8]]
        resultat = produit_matriciel(matrice1, matrice2)
        self.assertEqual(resultat, [[19, 22], [43, 50]])


if __name__ == '__main__':
    unittest.main()
